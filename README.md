# Zkoumání vzácnosti komunitních předmětů na platformě Steam

Ve slože scrapers se nachází 3 scrapery, požadavky na knihovny se nachází v souboru requirements.txt.
Scrapery lze jednoduše spustit pomocí bat skriptů v té samé složce.

Pořadí spuštění scraperů a jejich funkce:

1. Skript **acc_scraper** rekurzivně pomocí knihovny scrapy prochází přátele počátečního Steam účtu a ukládá jejich ID do souboru accounts.json ve složce output_data (scrapuje se přímo z HTML webu, jelikož zde narozdíl od API není rate limit).
2. Skript **inv_scraper** pomocí Steam API prochází inventáře účtů získaných předchozím skriptem, od každého komunitního předmětu ukládá počty do souboru inventories.csv ve složce output_data. Využívá protokolu TOR pro urychlení scrapování (API Steamu má rate limit asi 2 sekundy na request).
3. Skript **price_scraper** pomocí Steam API zjistí ceny pro všechny typy loot boxů (Cases a Sticker Capsules) získané v předchozím kroku a uloží je do souboru prices.csv ve složce output_data.

V souboru colab.py se pak nachází "očištěný" kód z Colabu. Na Colabu analyzujeme počty lootboxů a jejich celkové EUR hodnoty v inventářích.