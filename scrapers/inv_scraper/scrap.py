import os
import csv
from torpy.http.requests import tor_requests_session
import threading
import time
import json
import traceback

NUM_THREADS = 20

CSV_FILE = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..", "output_data", "inventories.csv"))
STEAMIDS_FILE = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..", "output_data", "accounts.json"))


class Colors:
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKCYAN = "\033[96m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


class Counter:
    def __init__(self):
        self.value = 0
        self._lock = threading.Lock()

    def inc(self):
        with self._lock:
            self.value += 1


class RateLimitExceededException(Exception):
    pass


def init_csv(file):
    csv_lock = threading.Lock()
    csv_file = open(file, "w", newline="", encoding="utf-8")
    csv_writer = csv.writer(csv_file, dialect="excel")
    csv_writer.writerow(["steamid", "item_name", "item_count", "scrap_time"])

    return csv_file, csv_writer, csv_lock


def steam_fetch_commodities(thread, sess, steamid):
    print(f"[{thread}] Fetching CSGO commodities for steamid {steamid}")
    response = sess.get(
        f"https://steamcommunity.com/inventory/{steamid}/730/2?l=english&count=5000"
    )
    if response.status_code == 200:
        json = response.json()

        if not "descriptions" in json:
            print(
                f"{Colors.WARNING}[{thread}] No CSGO inventory for steamid {steamid}{Colors.ENDC}"
            )
            return None

        names = {}
        for desc in json["descriptions"]:
            if desc["commodity"] == 1:
                names[desc["classid"]] = desc["name"]

        counts = {}
        for asset in json["assets"]:
            item_name = names.get(asset["classid"])

            if item_name:
                counts[item_name] = int(counts.get(item_name) or 0) + int(
                    asset["amount"]
                )

        if len(counts) > 0:
            print(
                f"{Colors.OKGREEN}[{thread}] Fetched {len(counts)} CSGO commodity types for steamid {steamid}{Colors.ENDC}"
            )
            return counts
        else:
            print(
                f"{Colors.WARNING}[{thread}] No CSGO commodities for steamid {steamid}{Colors.ENDC}"
            )
            return None

    elif response.status_code == 429:
        print(
            f"{Colors.FAIL}[{thread}] Hit rate limit ({response.status_code}) while fetching CSGO commodities for steamid {steamid}, killing Tor session{Colors.ENDC}"
        )
        raise RateLimitExceededException
    elif response.status_code == 403:
        print(
            f"{Colors.WARNING}[{thread}] Failed to fetch CSGO commodities for steamid {steamid} server returned {response.status_code} (Inventory private?){Colors.ENDC}"
        )
        return None
    else:
        print(
            f"{Colors.FAIL}[{thread}] Failed to fetch CSGO commodities for steamid {steamid} server returned {response.status_code}, killing Tor session{Colors.ENDC}"
        )
        raise Exception


def request_thread(thread, steamids, csv_file, csv_writer, csv_lock, counter):
    while steamids:
        print(f"[{thread}] Creating new Tor session")
        with tor_requests_session() as sess:
            try:
                while steamids:
                    account = steamids.pop(0)
                    steamid = account["steamid"]
                    counts = steam_fetch_commodities(thread, sess, steamid)

                    if counts is not None and len(counts) > 0:
                        current_time = time.strftime(r"%Y-%m-%d %H:%M:%S")

                        with csv_lock:
                            for key, val in counts.items():
                                csv_writer.writerow(
                                    [
                                        steamid,
                                        key,
                                        val,
                                        current_time,
                                    ]
                                )

                            csv_file.flush()

                    account = None
                    counter.inc()
                    time.sleep(2)
            except:
                traceback.print_exc()
                if account:
                    steamids.insert(0, account)


def info_thread(steamids, counter):
    total_count = len(steamids)

    while steamids:
        done_count = counter.value
        percent = (done_count / total_count) * 100
        print(
            f"{Colors.OKCYAN}[Info] {percent:.2f}% ({done_count}/{total_count}) of steamids scraped{Colors.ENDC}"
        )
        time.sleep(1)


def main():
    json_file = open(STEAMIDS_FILE)
    steamids = json.load(json_file)
    csv_file, csv_writer, csv_lock = init_csv(CSV_FILE)
    counter = Counter()

    threads = []

    thread = threading.Thread(
        target=info_thread,
        args=(steamids, counter),
    )
    threads.append(thread)
    thread.start()

    for i in range(NUM_THREADS):
        thread = threading.Thread(
            target=request_thread,
            args=(
                f"Thread-{(i+1):02}",
                steamids,
                csv_file,
                csv_writer,
                csv_lock,
                counter,
            ),
        )
        threads.append(thread)
        thread.start()

    for thread in threads:
        thread.join()

    csv_file.close()
    json_file.close()


if __name__ == "__main__":
    main()
