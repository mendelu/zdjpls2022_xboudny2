import scrapy


class SteamidSpider(scrapy.Spider):
    name = "steamids"
    logged = set()

    start_urls = [
        "https://steamcommunity.com/profiles/76561198052244200/friends/",
    ]

    def parse(self, response):
        next_pages = []

        for friend_block in response.css(".friend_block_v2"):
            steamid = friend_block.css("::attr(data-steamid)").get()
            if steamid not in self.logged:
                self.logged.add(steamid)
                link = friend_block.css("a::attr(href)").get()
                # print("REMOVED NEXT LINK IN SPIDER")
                next_pages.append(link)
                yield {
                    "steamid": steamid,
                    "display_name": friend_block.css(
                        ".friend_block_content::text"
                    ).get(),
                    "link": link,
                }

        for next_page in next_pages:
            next_page = response.urljoin(next_page + "/friends/")
            yield scrapy.Request(next_page, callback=self.parse)
