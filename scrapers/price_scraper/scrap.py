from concurrent.futures import thread
import os
import csv
import requests
import urllib.parse
import json
import time
import math

INPUT_CSV_FILE = os.path.abspath(
    os.path.join(
        os.path.dirname(__file__), "..", "..", "output_data", "inventories.csv"
    )
)
OUTPUT_CSV_FILE = os.path.abspath(
    os.path.join(os.path.dirname(__file__), "..", "..", "output_data", "prices.csv")
)


def ceil(number, digits):
    return math.ceil((10.0**digits) * number) / (10.0**digits)


def init_in_csv(file):
    csv_file = open(file, newline="", encoding="utf-8")
    csv_reader = csv.reader(csv_file, dialect="excel")

    return csv_file, csv_reader


def init_out_csv(file):
    csv_file = open(file, "w", newline="", encoding="utf-8")
    csv_writer = csv.writer(csv_file, dialect="excel")
    csv_writer.writerow(["item_name", "price_eur"])

    return csv_file, csv_writer


def main():
    in_file, reader = init_in_csv(INPUT_CSV_FILE)
    out_file, writer = init_out_csv(OUTPUT_CSV_FILE)

    next(reader)  # Skip header
    item_names = set(
        row[1]
        for row in reader
        if (
            row[1].endswith("Case")
            or "Capsule" in row[1]
            or row[1].startswith("2020 RMR")
        )
        and not "Key" in row[1]
    )

    print(f"Getting prices for {len(item_names)} items.")

    for item_name in item_names:
        time.sleep(2)
        hash_name = urllib.parse.quote(item_name, safe="")

        response = requests.get(
            f"http://steamcommunity.com/market/priceoverview/?appid=730&market_hash_name={hash_name}&currency=3"
        )

        while response.status_code == 429:
            print(f"Waiting for rate limit to pass")
            time.sleep(20)
            response = requests.get(
                f"http://steamcommunity.com/market/priceoverview/?appid=730&market_hash_name={hash_name}&currency=3"
            )

        if response.status_code == 200:
            response_json = response.json()

            if response_json["success"]:
                price = float(
                    response_json["lowest_price"]
                    .replace("€", "")
                    .replace(",", ".")
                    .replace("-", "0")
                )
                price_adj = round(price * 1.05, 2)
                print(f"Got price for {item_name} ({price_adj})")
                writer.writerow([item_name, price_adj])
            else:
                print(f"Failed to get price for {item_name} - Steam reports fail")
        else:
            print(
                f"Failed to get price for {item_name} - Status code {response.status_code}"
            )

    in_file.close()
    out_file.close()


if __name__ == "__main__":
    main()
